package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {

    private final List<Product> products_list;

    public ShoppingCart() {
        this.products_list = new ArrayList<>();
    }

    public boolean addProducts(String name, int price, int quantity) {
        if(getAllProductsQuantity() + quantity >= ShoppingCart.PRODUCTS_LIMIT || price <= 0 || quantity <= 0){
            return false;
        }
        name = name.toLowerCase();

        if(isProductAlreadyAdded(name)){
            for(Product product: products_list){
                if(product.getName().equals(name) && product.getPrice() != price){
                    product.setAmount(product.getAmount() + quantity);
                    return true;
                }
            }
        }else{
            products_list.add(new Product(name, price, quantity));
            return true;
        }
        return false;
    }

    public boolean deleteProducts(String name, int quantity) {
        if(quantity <=0 ){
            return false;
        }
        name = name.toLowerCase();
        for(Product product: products_list){
            if(product.getName().equals(name)){
                if(product.getAmount() > quantity){
                    product.setAmount(product.getAmount() - quantity);
                    return true;
                }else if(product.getAmount() == quantity){
                    products_list.remove(product);
                    return true;
                }

            }
        }
        return false;
    }

    public int getQuantityOfProduct(String name) {
        return products_list.stream()
                .filter(product -> product.getName().equals(name.toLowerCase()))
                .mapToInt(Product::getAmount).sum();
    }

    public int getSumProductsPrices() {
        return products_list.stream().mapToInt(product -> product.getPrice() * product.getAmount()).sum();
    }

    public int getProductPrice(String name) {
        name = name.toLowerCase();

        for(Product product: products_list){

            if(product.getName().equals(name)){
                return product.getPrice();
            }
        }
        return 0;
    }

    public List<String> getProductsNames() {
        return products_list.stream().map(product ->
                product.getName().substring(0,1).toUpperCase() + product.getName().substring(1))
                .collect(Collectors.toList());

    }

    private boolean isProductAlreadyAdded(String name){
        return products_list.stream()
                .anyMatch(product -> product.getName()
                        .equals(name));
    }

    private  int getAllProductsQuantity(){
        return products_list.stream()
                .mapToInt(product -> product.getAmount())
                .sum();
    };
}
