package pl.edu.pwsztar;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

//TODO: Podczas implementacji prosze pamietac o zasadzie F.I.R.S.T
public class ShoppingCartTest {

    @DisplayName("Should add product.")
    @ParameterizedTest
    @CsvSource({
            "Chips, 4, 1",
            "Pasta, 5, 2",
            "Banana, 2, 3"
    })
    void shouldAddProductToShoppingCart(String name, int price, int amount) {
        ShoppingCart shoppingCart = new ShoppingCart();
        boolean result = shoppingCart.addProducts(name, price, amount);

        assertTrue(result);
    }

    @DisplayName("Should not add product.")
    @ParameterizedTest
    @CsvSource({
            "Chips, -4, 1",
            "Pasta, -5, 2",
            "Banana, -2, 3"
    })
    void shouldNotAddProductToShoppingCart(String name, int price, int amount) {
        ShoppingCart shoppingCart = new ShoppingCart();
        boolean result = shoppingCart.addProducts(name, price, amount);

        assertFalse(result);
    }

    @DisplayName("Should delete product.")
    @ParameterizedTest
    @CsvSource({
            "Chips, 5",
            "Pasta,  5",
            "Banana, 5"
    })
    void shouldDeleteProductFromShoppingCart(String name, int amount) {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Chips", 4, 10);
        shoppingCart.addProducts("Pasta", 5, 10);
        shoppingCart.addProducts("Banana", 2, 10);

        boolean result = shoppingCart.deleteProducts(name, amount);
        assertTrue(result);
    }

    @DisplayName("Shouldn't delete product from Shopping Cart")
    @ParameterizedTest
    @CsvSource({
            "Chips, 5",
            "Pasta,  5",
            "Banana, 5"
    })
    void shouldNotDeleteProductFromShoppingCart(String name, int amount) {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Chips", 4, 1);
        shoppingCart.addProducts("Pasta", 5, 1);
        shoppingCart.addProducts("Banana", 2, 1);

        boolean result = shoppingCart.deleteProducts(name, amount);
        assertFalse(result);
    }

    @DisplayName("Should return number of product.")
    @ParameterizedTest
    @CsvSource({
            "Chips, 5",
            "Pasta,  5",
            "Banana, 5"
    })
    void shouldReturnQuantityOfProductFromShoppingCart(String name, int amount) {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Chips", 4, 5);
        shoppingCart.addProducts("Pasta", 5, 5);
        shoppingCart.addProducts("Banana", 2, 5);

        int result = shoppingCart.getQuantityOfProduct(name);
        assertEquals(result, amount);
    }

    @DisplayName("Should return whole sum price.")
    @Test
    void shouldReturnSumOfProductPriceFromShoppingCart() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Chips", 4, 5);
        shoppingCart.addProducts("Pasta", 5, 5);
        shoppingCart.addProducts("Banana", 2, 5);

        int result = shoppingCart.getSumProductsPrices();
        assertEquals(55, result);
    }

    @DisplayName("Should return price of product.")
    @ParameterizedTest
    @CsvSource({
            "Chips, 4",
            "Pasta,  5",
            "Banana, 2"
    })
    void shouldReturnProductPriceFromShoppingCart(String name, int price) {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Chips", 4, 5);
        shoppingCart.addProducts("Pasta", 5, 5);
        shoppingCart.addProducts("Banana", 2, 5);

        int result = shoppingCart.getProductPrice(name);
        assertEquals(result, price);
    }

    @DisplayName("Should return names of products.")
    @Test
    void shouldReturnNamesOfProductFromShoppingCart() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Chips", 4, 5);
        shoppingCart.addProducts("Pasta", 5, 5);
        shoppingCart.addProducts("Banana", 2, 5);

        List<String> products = new ArrayList<>();
        products.add("Chips");
        products.add("Pasta");
        products.add("Banana");

        List<String> result = shoppingCart.getProductsNames();
        assertEquals(products, result);
    }
}
